import java.util.*;

public class Blocked extends Estado{

	private int numDispositivos;
	private ArrayList<Dispositivo> dispositivos = new ArrayList<Dispositivo>();

	public Blocked(int n, String[] tokens){
		numDispositivos=n;

		int nid=1;
		for(int i=2;i<=tokens.length-1;i++){
			dispositivos.add(new Dispositivo(nid,Integer.parseInt(tokens[i])));
			nid++;
		}
	}

	// retorna a lista de dispositivos
	public ArrayList<Dispositivo> getDispositivos(){
		return dispositivos;
	}

	// retorna o dispositivo com o id do index
	public Dispositivo getDispositivo(int index){
		return dispositivos.get(index);
	}

	// coloca na fila do dispositivo de destino [p.getDestino()-1] o processo p
	public void enqueue(Processo p){
		//System.out.println("*** destino do processo: "+p.getResto().get(0));
		Simulador.output += "vai fazer enqueue do "+p+" no dispositivo "+p.getDestino()+"\n";
		dispositivos.get(p.getDestino()-1).enqueue(p);
	}

	public String toString(){
		String output="";
		Iterator<Dispositivo> it = dispositivos.iterator();
		while(it.hasNext()){
			Dispositivo d = it.next();
			output += "Dispositivo "+d.getID()+" {"+d.toString()+"}\n";
		}

		return output;
	}
}