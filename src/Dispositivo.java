import java.util.*;

public class Dispositivo{

	private int numid, tempodeacesso, contadordeacesso;
	private Queue<Processo> processos;

	public Dispositivo(int n, int t){
		numid=n;
		tempodeacesso=t;
		contadordeacesso=t;
		processos = new LinkedList<Processo>();
	}

	// decrementa 1 no contador de acesso do dispositivo se tiver processos, se não tiver e este não for igual ao tempo de acesso definido do dispositivo iguala-o ao mesmo
	public void clock(){
		if(!processos.isEmpty()){
			contadordeacesso--;
			Simulador.output += "vai decrementar contador de acesso para: "+contadordeacesso+"\n";
		}
		else
			if(contadordeacesso!=tempodeacesso){
				contadordeacesso=tempodeacesso;
				Simulador.output += "contador de acesso vai igualar ao tempo de acesso\n";
			}
	}

	// retorna o id do dispositivo
	public int getID(){
		return numid;
	}

	// retorna o contador de acesso
	public int getContador(){
		return contadordeacesso;
	}

	// retorna o primeiro processo da fila e faz reset ao clock para (se houver outro processo na fila) recomeçar a contagem
	public Processo dequeue(){
		resetClock();
		return processos.poll();
	}

	// coloca um processo na fila do dispositivo
	public void enqueue(Processo p){
		if(p.getNextTempo()==0)
			p.removeNextTempo();
		processos.offer(p);
	}

	// faz reset ao contador de acesso, igualando-o ao tempo de acesso definido do dispositivo
	public void resetClock(){
		contadordeacesso=tempodeacesso;
	}

	// retorna a lista de processos
	public ArrayList<Processo> getProcessos(){
		ArrayList<Processo> procs = new ArrayList<Processo>();
		Iterator<Processo> it = processos.iterator();
		while(it.hasNext())
			procs.add(it.next());
		return procs;
	}

	// retorna o processo que está na cabeça da fila do dispositivo
	public Processo peekQueue(){
		return processos.peek();
	}

	public String toString(){
		String output="tempodeacesso: "+tempodeacesso+" [processos: ";
		Iterator<Processo> it = processos.iterator();
		if(it.hasNext()){
			while(it.hasNext()){
				Processo p = it.next();
				if(p!=null)
					output += p.toString()+"; ";
				else
					output += "tem null";
			}
			output+="] cont: "+contadordeacesso;
		}
		else
			output+="vazio]";
		return output;
	}
}