import java.util.*;

public class Ready extends Estado{

	int limite;

	public Ready(){
		limite=2;
		super.queue = new LinkedList<Processo>();
	}

	public Ready(int l){
		limite=l;
		super.queue = new LinkedList<Processo>();
	}

	// retorna o limite da fila ready
	public int getLimite(){
		return limite;
	}

	// altera o limite da fila ready
	public void setLimite(int l){
		limite=l;
	}

	// retorna true se a fila ready está cheia
	public boolean isFull(){
		return queue.size()==limite;
	}

	// retorna true se a fila ready está vazia
	public boolean isEmpty(){
		//System.out.println("ready is empty? "+queue.isEmpty()+", tem "+queue.peek());
		return queue.isEmpty();
	}

	// retorna os processos que estão na fila ready
	public ArrayList<Processo> getProcessos(){
		ArrayList<Processo> procs = new ArrayList<Processo>();
		Iterator<Processo> it = queue.iterator();
		while(it.hasNext())
			procs.add(it.next());
		return procs;
	}

	public String toString(){
		String output="";
		Iterator<Processo> it = this.queue.iterator();
		if(it.hasNext()){
			while(it.hasNext()){
				Processo p = it.next();
				if(p!=null)
					output += "Processo {"+p.toString()+"}\n";
				else
					output += "tem null\n";
			}
		}
		else
			output += "vazio\n";
		return output;
	}
}