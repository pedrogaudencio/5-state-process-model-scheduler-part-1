import java.util.*;

public class Processo{

    private int cpuini, cpufim, tempowait, tempototal;
    private boolean flag;
    private ArrayList<Tempos> resto = new ArrayList<Tempos>();

    public Processo(int i, ArrayList<Tempos> r, int f){
        cpuini = i;
        resto = r;
        cpufim = f;
        tempowait = 0;
        flag = true;

        for(Tempos tempos : resto)
            tempototal += tempos.getTempo();
        tempototal += cpufim;
    }

    // retorna o tempo de inicio do cpu
    public int getCPUinit(){
        return cpuini;
    }

    // retorna o tempo de fim do cpu
    public int getCPUfim(){
        return cpufim;
    }

    // retorna a lista de tuplos {tempo, dispositivo}
    public ArrayList<Tempos> getResto(){
        return resto;
    }

    // retorna true se o tempo do processo para o primeiro dispositivo já chegou a zero
    public boolean tempoIsZero(){
        return resto.get(0).getTempo() == 0;
    }

    // retorna o primeiro dispositivo de destino
    public int getDestino(){
        return resto.get(0).getDispositivo();
    }

    // retorna o proximo tempo para o primeiro dispositivo
    public int getNextTempo(){
        return resto.get(0).getTempo();
    }

    // remove o primeiro {tempo, dispositivo de destino} do processo, é apenas usado quando este é zero
    public void removeNextTempo(){
        resto.remove(0);
    }

    // retorna true se o processo já não tem nada a fazer
    public boolean isProcessed(){
    	return resto.isEmpty() && cpufim==0;
    }

    // decrementa tempo de processamento no primeiro {tempo, dispositivo de destino}, se este já estiver processado decrementa no tempo final de cpu
    public void decrementa(){
        if(!resto.isEmpty())
            resto.get(0).decrementaTempo();
        else
            cpufim--;
    }

    // incrementa no tempo de espera (fila ready)
    public void tempoWait(){
        tempowait++;
    }

    // retorna tempo de espera
    public int getTempoWait(){
        return tempowait;
    }

    // retorna tempo total
    public int getTempoTotal(){
        return tempototal;
    }

    // retorna flag
    public boolean getFlag(){
        return flag;
    }

    // altera a flag
    public void setFlag(boolean f){
        flag = f;
    }

    public String toString(){
        String output = "";
        Iterator <Tempos> it = resto.iterator();
        output += "(pcb - ti:" + cpuini + " | ";
        while(it.hasNext()){
            Tempos t = it.next();
            output += t.toString() + " ";
        }
        output += " | tf:" + cpufim + ")";
        return output;
    }
}
