public class Tempos{

	private int dispositivo,tempo;

	public Tempos(int t, int d){
		tempo=t;
		dispositivo=d;
	}

	// retorna o dispositivo
	public int getDispositivo(){
		return dispositivo;
	}

	// retorna o tempo
	public int getTempo(){
		return tempo;
	}

	// altera o tempo
	public void setTempo(int t){
		tempo=t;
	}

	// decrementa 1 no tempo
	public void decrementaTempo(){
		tempo--;
	}

	public String toString(){
		return new String("t:"+tempo+",d:"+dispositivo);
	}
}