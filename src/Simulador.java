import java.util.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Simulador{

	//estados
	New novo = new New();
	Ready ready = new Ready();
	Running running = new Running();
	Blocked blocked;
	Exit exit = new Exit();

	//input
	int numProcessos, clock;
	ArrayList<Tempos> listatempos;
	Queue<Processo> listaProcessos = new LinkedList<Processo>();

	FileOutputStream fop = null;
	File file;
	static String output = "";


	public Simulador(ArrayList<String> listainput){
		//1a linha
		String linha1=listainput.remove(0);
		String[] tokens = linha1.split(" ");

		numProcessos=Integer.parseInt(tokens[0]);
		int numDispositivos=Integer.parseInt(tokens[1]);

		//cria dispositivos
		blocked = new Blocked(numDispositivos, tokens);

		//cria lista de processos
		while(!listainput.isEmpty()){
			listatempos = new ArrayList<Tempos>();

			String linha2=listainput.remove(0);
			String[] tokens2 = linha2.split(" ");

			int tini=Integer.parseInt(tokens2[0]);
			int tfim=Integer.parseInt(tokens2[tokens2.length-1]);

			for(int j =1;j<tokens2.length -1;j++){
				//System.out.println("Pares: "+tokens2[j]+" "+tokens2[j+1]);
				listatempos.add(new Tempos(Integer.parseInt(tokens2[j]),Integer.parseInt(tokens2[j+1])));
				j++;
			}
			listaProcessos.offer(new Processo(tini,listatempos,tfim));
		}
	}

	// metodo principal para ciclar o simulador
	public void scheduler(){
		clock=0;
		while(exit.getQueue().size()!=numProcessos){

			if(!listaProcessos.isEmpty() && listaProcessos.peek().getCPUinit()==clock)
				novo.enqueue(listaProcessos.poll());

			printStatus(clock);
			collectOutput();

			event_occurs();

			// run
			if(running.isEmpty() || running.isNull()){
				if(!ready.isEmpty()){
					if(ready.getQueue().peek().getFlag()){
						dispatch(); // saca do ready e mete no running
						for(Processo p : ready.getProcessos())
								p.setFlag(true);
					}
					else{
						for(Processo p : ready.getProcessos())
							p.setFlag(true);
					}
				}
			}
			else{
				if(!running.isEmpty() && !running.isNull()){
					if(running.next().isProcessed()){
						running.resetTimeout();
						release(); // saca do running e mete no exit
					}
					else{
						if(!running.next().getResto().isEmpty() && running.next().getNextTempo()==0){
							running.resetTimeout();
							event_wait(); // saca do running e mete no blocked
						}
						else{
							if(running.isZero()){
								timeout(); // saca do running e mete no ready
								running.resetTimeout();
							}
						}
					}
				}
			}
			admit();

			tictac();
		}

		printStatus(clock);
		printDetalhesProcessamento();

		collectOutput();
		writeToFile();
	}

	// retira o processo da fila new e coloca na fila ready
	public void admit(){
		if(!ready.isFull() && novo.getQueue().peek()!=null){
			ready.enqueue(novo.dequeue());
			output += "vai fazer admit()\n";
		}
	}

	// retira o processo da fila ready e coloca na fila running
	public void dispatch(){
		running.enqueue(ready.dequeue());
		output += "vai fazer dispatch()\n";
	}

	// retira o processo da fila running e coloca na fila ready
	public void timeout(){
		ready.enqueue(running.dequeue());
		output += "vai fazer timeout()\n";
	}

	// retira o processo da fila running e coloca na fila exit
	public void release(){
		exit.enqueue(running.dequeue());
		output += "vai fazer release()\n";
	}

	// retira o processo da fila running e coloca na fila blocked (no dispositivo de destino)
	public void event_wait(){
		blocked.enqueue(running.dequeue());
		output += "vai fazer event_wait()\n";
	}

	// retira o processo da fila blocked (do dispositivo respetivo) e coloca na fila ready
	public void event_occurs(){
		Iterator<Dispositivo> it = blocked.getDispositivos().iterator();
		while(it.hasNext()){
			Dispositivo d = it.next();
			if(d.getContador()==0){ // se o contador interno do dispositivo for zero, saca do dispositivo e mete no ready
				output += "vai fazer event_occurs() - enqueue do processo do dispositivo "+d.getID()+" na fila ready\n";
				d.peekQueue().setFlag(false);
				ready.enqueue(d.dequeue());
			}
		}
	}

	// metodo para incrementação e decrementação temporal de todo o scheduler
	public void tictac(){
		// processo no running
		if(!running.isNull()){
			running.decrementaTimeout();
			running.next().decrementa();
		}
		// clock do dispositivo
		for(Dispositivo d : blocked.getDispositivos())
			d.clock();
		// acumula no tempo de espera dos processos na fila ready
		for(Processo p : ready.getQueue())
				p.tempoWait();
		clock++;
	}

	// imprime output na consola
	public void printStatus(int t){
		String s = "< "+t+" > \t";
		for(Processo p : novo.getQueue())
			s += "<P"+p.getCPUinit()+"-[new]>  ";

		for(Processo p : ready.getQueue())
			s += "<P"+p.getCPUinit()+"-[ready]>  ";

		for(Dispositivo d : blocked.getDispositivos())
			for(Processo p : d.getProcessos())
				s += "<P"+p.getCPUinit()+"-[blocked]>  ";

		for(Processo p : running.getQueue())
			s += "<P"+p.getCPUinit()+"-[running]>  ";

		for(Processo p : exit.getQueue())
			s += "<P"+p.getCPUinit()+"-[exit]> ";

		System.out.println(s);
	}

	// analisa os resultados de processamento e imprime na consola
	public void printDetalhesProcessamento(){
		System.out.println("");
		for(Processo p : exit.getQueue())
			System.out.printf("<relação entre tempo de espera e tempo CPU de P%d: %f>\n", p.getCPUinit(), (float) p.getTempoTotal()/p.getTempoWait());
	}

	// metodo verboso para output.txt
	public void collectOutput(){
		output += "\n*************************************************************************************\nclock="+
							clock+"\n*************************************************************************************\n\n";
		output += "-------------------------\nnew:\n"+novo.toString()+"\n";
		output += "-------------------------\nready:\n"+ready.toString()+"\n";
		output += "-------------------------\nblocked:\n"+blocked.toString()+"\n";
		output += "-------------------------\nrunning:\n"+running.toString()+"\n";
		output += "-------------------------\nexit:\n"+exit.toString()+"\n";
	}

	// escreve output detalhado de cada ciclo num ficheiro output.txt
	public void writeToFile(){
		try{
		file = new File("output.txt");
		fop = new FileOutputStream(file);

		if(!file.exists())
			file.createNewFile();

			byte[] content = output.getBytes();
 
			fop.write(content);
			fop.flush();
			fop.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (fop != null) {
					fop.close();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}